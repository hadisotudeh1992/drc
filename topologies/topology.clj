(ns wordcount
  (:use     [streamparse.specs])
  (:gen-class))

(defn wordcount [options]
   [
    ;; spout configuration
    {"url-spout" (python-spout-spec
          options
          "spouts.url.Url"
          ["url" "rate"]
          )
    }
    ;; bolt configuration
    {"sleeper-bolt" (python-bolt-spec
          options
          {"url-spout" :shuffle}
          "bolts.sleeper.Sleeper"
          {"digikala-stream" ["digikala"]
          "chareh-stream" ["chareh"]
          "other-stream" ["other"]}
          :p 2
          )
    "download-bolt-1" (python-bolt-spec
          options
          {["sleeper-bolt" "digikala-stream"] ["digikala"]
          ["sleeper-bolt" "other-stream"] ["other"]}
          "bolts.download.Download"
          []
          :p 2
          )
    "download-bolt-2" (python-bolt-spec
          options
          {["sleeper-bolt" "chareh-stream"] ["chareh"]
          ["sleeper-bolt" "other-stream"] ["other"]}
          "bolts.download.Download"
          []
          :p 2
          )
    }
  ]
)
